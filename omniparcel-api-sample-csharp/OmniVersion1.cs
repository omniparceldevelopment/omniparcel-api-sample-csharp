﻿using Newtonsoft.Json;
using omniparcel_api_sample_csharp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace omniparcel_api_sample_csharp
{
    class OmniVersion1
    {
        HttpClient client;
        MediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();

        public OmniVersion1()
        {
            client = new HttpClient();
            client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiBasePath"]);
            client.DefaultRequestHeaders.Add("access_key", ConfigurationManager.AppSettings["ApiAccessKey"]);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        internal void Run()
        {
            string connote = PrintShipment();

            ReprintConsignment(connote);

            // For sample, we're only submitting 1 connote number, on a live system, this will normally be done at the end of day, for all shipments in a batch.
            SendManifest(connote);
        }

        
        private string PrintShipment()
        {
            var post = new
            {
                DeliveryReference = "ORDER123",
                Destination = new
                {
                    Name = "DestinationName",
                    Address = new
                    {
                        BuildingName = "",
                        StreetAddress = "DestinationStreetAddress",
                        Suburb = "Mascot",
                        City = "NSW",
                        PostCode = "2020",
                        CountryCode = "AU",
                    },
                    ContactPerson = "DestinationContact",
                    PhoneNumber = "123456789",
                    Email = "destinationemail@email.com",
                    DeliveryInstructions = "Desinationdeliveryinstructions"
                },
                IsSignatureRequired = true,
                Packages = new List<object>(new[] { new { 
                    Height = 10, 
                    Length = 10, 
                    Width = 10, 
                    Kg = 1.5M, 
                    Name = "Custom Box", 
                    Type = "Box"
                }
                }),
                Commodities = new List<object>(new[] 
                {
                    new { Description = "Apparel", UnitKg = 1.5M, UnitValue = 1, Units = 1, Country = "CN", Currency = "AUD" }
                }),
                Outputs = new List<object>(new string[] { "LABEL_PDF", "LABEL_PNG" }), 
                PrintToPrinter = false
            };


            HttpResponseMessage response = client.PostAsJsonAsync("labels/printshipment", post).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var data = response.Content.ReadAsStringAsync().Result;

                var connote = "";

                if (!string.IsNullOrEmpty(data) && data != "null")
                {
                    var lines = JsonConvert.DeserializeObject<CreateShipmentResponse>(data);

                    foreach (var item in lines.Consignments)
                    {
                        Console.WriteLine("{0}", item.Connote);

                        Open_Output_FIles(item);

                        connote = item.Connote;
                    }
                    foreach (var item in lines.Errors)
                    {
                        Console.WriteLine("{0} --> {1}", item.Property, item.Message);
                    }

                    return connote;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                Console.ForegroundColor = ConsoleColor.White;
            }
            return string.Empty;
        }

        private static void Open_Output_FIles(CreateShipmentResponse.ConnoteSummary item)
        {
            if (item.OutputFiles != null)
            {
                foreach (var ot in item.OutputFiles)
                {
                    foreach (var att in ot.Value)
                    {
                        string file = "";
                        if (ot.Key.Contains("PNG"))
                            file = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\" + Guid.NewGuid() + ".png";
                        else
                            file = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\" + Guid.NewGuid() + ".pdf";

                        System.IO.File.WriteAllBytes(file, att);

                        System.Diagnostics.Process.Start(file);
                    }
                }
            }
        }

        private void ReprintConsignment(string connote)
        {
            Console.WriteLine("===== {0} =====", "POST ReprintConsignment");
            Console.WriteLine("");


            HttpResponseMessage response = client.PostAsync(string.Format("labels/reprint?consignmentno={0}", connote), null).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var data = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(data);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                Console.ForegroundColor = ConsoleColor.White;
            }

        }

        private void SendManifest(string connote)
        {
            Console.WriteLine("===== {0} =====", "POST publishmanifest");
            Console.WriteLine("");

            // For sample, we're only submitting 1 connote number, on a live system, this will normally be done at the end of day, for all shipments in a batch.
            List<string> connotes = new List<string>();
            connotes.Add(connote);

            HttpResponseMessage response = client.PostAsJsonAsync("labels/publishmanifest", connotes).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var data = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(data) && data != "null")
                {
                    var lines = JsonConvert.DeserializeObject<Dictionary<string, byte[]>>(data);

                    foreach (var item in lines)
                    {
                        var file = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\" + item.Key + "_" + Guid.NewGuid() + ".pdf";

                        System.IO.File.WriteAllBytes(file, item.Value);

                        System.Diagnostics.Process.Start(file);
                    }
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

    }
}

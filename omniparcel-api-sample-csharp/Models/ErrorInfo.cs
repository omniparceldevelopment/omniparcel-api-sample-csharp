﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace omniparcel_api_sample_csharp.Models
{
    public class ErrorInfo
    {
        public string Property { get; set; }
        public string Message { get; set; }

    }
}
